package com.example.todo;

public class TaskModel {
    private int id;
    private String name;
    private String description;
    private boolean isFinished;

    public TaskModel(int id, String name, String description, boolean isFinished) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.isFinished = isFinished;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }
}
