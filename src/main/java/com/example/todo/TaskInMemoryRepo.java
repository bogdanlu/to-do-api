package com.example.todo;

import java.util.ArrayList;
import java.util.List;

public class TaskInMemoryRepo {
    private List<TaskModel> tasks;

    public TaskInMemoryRepo() {
        tasks = new ArrayList<>();
        tasks.add(new TaskModel(1, "Luca", "create tutorial", true));
        tasks.add(new TaskModel(2, "Bogdan", "create API", false));
        tasks.add(new TaskModel(3, "Sebi", "football training", true));
        tasks.add(new TaskModel(4, "Dragos", "move to Cluj", false));
        tasks.add(new TaskModel(5, "Cosmin", "QR code app", true));
        tasks.add(new TaskModel(6, "Lisa", "add discount logic", true));
        tasks.add(new TaskModel(7, "Andrei", "update test plan", false));
        tasks.add(new TaskModel(8, "Dragos", "review release notes", false));
        tasks.add(new TaskModel(9, "Florin", "1-to-1 meeting", true));
        tasks.add(new TaskModel(10, "Andrei", "test log4j upgrade", false));
        tasks.add(new TaskModel(11, "Bogdan", "try new java 8 jdk", true));
        tasks.add(new TaskModel(12, "Alex", "performance testing", true));
        tasks.add(new TaskModel(13, "Bogdan", "create new sprint", false));
        tasks.add(new TaskModel(14, "Alex", "add new template", false));
        tasks.add(new TaskModel(15, "Sebi", "add searchable dropdown", true));
        tasks.add(new TaskModel(16, "Roxana", "create presentation", false));
        tasks.add(new TaskModel(17, "Mara", "send documentation", true));
        tasks.add(new TaskModel(18, "Catalin", "migrate PHP app", true));
    }

    public List<TaskModel> getTasks() {
        return this.tasks;
    }

    public void addTask(TaskModel task) {
        task.setId(this.getMaxId() + 1);
        this.tasks.add(task);
    }
    private int getMaxId() {
        int maxId = -1;
        for (TaskModel taskModel : this.tasks) {
            if (maxId < taskModel.getId()) {
                maxId = taskModel.getId();
            }
        }

        return maxId;
    }

    public void deleteTask(int id) {
        for(TaskModel task : this.tasks) {
            if (task.getId() == id) {
                this.tasks.remove(task);
                break;
            }
        }
    }

    public void updateTask(TaskModel taskToUpdate) {
        for(TaskModel task : this.tasks) {
            if (task.getId() == taskToUpdate.getId()) {
                task.setName(taskToUpdate.getName());
                task.setDescription(taskToUpdate.getDescription());
                task.setFinished(taskToUpdate.isFinished());
                break;
            }
        }
    }
}
