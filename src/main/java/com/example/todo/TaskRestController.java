package com.example.todo;

import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin(origins = "*")
@RestController
public class TaskRestController {
    private final TaskInMemoryRepo repository;

    public TaskRestController() {
        repository = new TaskInMemoryRepo();
    }

    @GetMapping("/tasks")
    List<TaskModel> all() {
        return repository.getTasks();
    }

    @PostMapping("/tasks")
    void newTask(@RequestBody TaskModel taskModel) {
        repository.addTask(taskModel);
    }

    @PutMapping("/tasks")
    void updateTask(@RequestBody TaskModel taskModel) {
        repository.updateTask(taskModel);
    }

    @DeleteMapping("/tasks/{id}")
    void deleteTask(@PathVariable int id) {
        repository.deleteTask(id);
    }

}
